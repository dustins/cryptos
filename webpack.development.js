const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
    mode: "development",
    
	context: path.resolve(__dirname, 'src/main/webapp'),
	
	entry: {
		app: "./app/index.tsx",
		vendor: ['react', 'react-dom']
	},
    
	resolve: {
		extensions: [ '.tsx', '.ts', '.jsx', '.js' ]
	},
	
	output: {
		filename: '[name].[chunkhash].js',
		path: path.resolve(__dirname, 'src/main/resources/static')
	},
	
	module: {
		rules: [
			{
				include: [path.resolve(__dirname, 'src')],
				loader: 'babel-loader',

				options: {
					plugins: ['syntax-dynamic-import'],

					presets: [
						[
							'env',
							{
								modules: false
							}
						]
					]
				},

				test: /\.js$/
			},
			{
				test: /\.(scss|css)$/,

				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			}
		]
	},
	
	plugins: [
		new CleanWebpackPlugin(['src/main/resources/static']),
		new HtmlWebpackPlugin({
	      title: 'Cryptos',
	      template: './public/index.html'
	    })
	]
}