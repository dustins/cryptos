package net.swigg.cryptos

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.content.*

class CryptosApp

fun Application.cryptosModule() {
    routing {
        static {
            resources("static")
            defaultResource("static/index.html")
        }
    }
}