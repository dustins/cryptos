import * as React from 'react'
import { throttleTime } from 'rxjs/operators'
import { CryptosContext, createCryptosContext } from './cryptosContext'
import { Trade } from './publicApiService'
import { PriceGraph } from './priceGraph'

interface CryptosState {
    bookSize: number,
    price: number,
    bid: number,
    ask: number
}

export class Cryptos extends React.Component<any, CryptosState> {
    static contextType = CryptosContext
    
    private cryptosContext: CryptosContext = createCryptosContext()
    
    constructor(props: any) {
      super(props);
      
      this.state = {
          bookSize: null,
          price: null,
          bid: null,
          ask: null
      }
      
      this.cryptosContext.publicApi.trades().subscribe((value: Trade) => {
          this.setState({price: value.price})
      })
      
      this.cryptosContext.publicApi.updates().pipe(throttleTime(2000)).subscribe(() => {
          this.setState({
              bookSize: this.cryptosContext.publicApi.book.size(),
              bid: this.cryptosContext.publicApi.book.bid(),
              ask: this.cryptosContext.publicApi.book.ask()
          })
      })
    }
    
    render() {
        return (
            <CryptosContext.Provider value={this.cryptosContext}>
                <dl>
                    <dt>Current Ask</dt>
                    <dd>{this.state.ask || "n/a"}</dd>
                    <dt>Current Bid</dt>
                    <dd>{this.state.bid || "n/a"}</dd>
                    <dt>Book Entries</dt>
                    <dd>{this.state.bookSize || "n/a"}</dd>
                    <dt>Last Trade Price</dt>
                    <dd>{this.state.price || "n/a"}</dd>
                </dl>
                <PriceGraph width={256} height={256} />
            </CryptosContext.Provider>
        )
    }
}
