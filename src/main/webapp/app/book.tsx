interface BookEntry {
    price: number,
    remaining: number,
    side: string
}

export class Book {
    private entries: {[key: string]: BookEntry} = {}
    
    clear(): void {
        this.entries = {}
    }
    
    bid(): number {
        return Object.keys(this.entries).map(key => {
            return this.entries[key]
        }).filter(entry => {
            return entry.side == "bid"
        }).map(entry => {
            return entry.price
        }).reduce((a, b) => {
            return Math.max(a,b)
        }, 0)
    }
    
    ask(): number {
        return Object.keys(this.entries).map(key => {
            return this.entries[key]
        }).filter(entry => {
            return entry.side == "ask"
        }).map(entry => {
            return entry.price
        }).reduce((a, b) => {
            return Math.min(a,b)
        }, Number.MAX_VALUE)
    }
    
    size(): number {
        return Object.keys(this.entries).length
    }
    
    addEntry(entry: BookEntry): void {
        // remove entries without any remaining
        if (entry.remaining == 0) {
            this.removeEntry(entry)
            return
        }
        
        // create or update entry
        if (this.entries[entry.price] == undefined) {
            // add new entry
            this.entries[entry.price] = {
                price: Number(entry.price),
                remaining: Number(entry.remaining),
                side: entry.side
            }
        } else {
            // update existing entry
            this.entries[entry.price].remaining = Number(entry.remaining)
        }
    }
    
    removeEntry(entry: BookEntry): void {
        delete this.entries[entry.price]
    }
}