import * as React from 'react'
import * as PIXI from 'pixi.js'

interface PriceGraphProps {
    width: number,
    height: number
}

export class PriceGraph extends React.Component<PriceGraphProps> {
    private app: PIXI.Application
    private priceGraph: any
    
    constructor(props: PriceGraphProps) {
        super(props)
        this.priceGraph = React.createRef()
        this.app = new PIXI.Application({
            width: this.props.width,
            height: this.props.height,
            antialias: true
        });
        
        setTimeout(() => {
            let rectangle = new PIXI.Graphics()
            rectangle.beginFill(0x66CCFF)
            rectangle.drawRoundedRect(0, 0, 64, 64, 5)
            rectangle.endFill()
            rectangle.x = 170
            rectangle.y = 170
            this.app.stage.addChild(rectangle);
            let last = window.performance.now()
            let targetFrame = (1/60*1000)
            
            let animate = (time: DOMHighResTimeStamp) => {
                rectangle.rotation+= (Math.PI*2/120) * ((time-last)/targetFrame)
                window.requestAnimationFrame(animate)
                last = time
            }
            
            animate(window.performance.now())
        }, 1000)
    }
    
    componentDidMount() {
        this.priceGraph.current.appendChild(this.app.view)
    }
    
    render() {
        return <div ref={this.priceGraph} />
    }
}