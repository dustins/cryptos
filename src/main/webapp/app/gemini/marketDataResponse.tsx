import { MarketEvent } from './marketEvent'

export interface AbstractMarketDataResponse {
    type: string,
    socket_sequence: number
}

export interface MarketHeartbeatResponse extends AbstractMarketDataResponse {}

export interface MarketUpdateResponse extends AbstractMarketDataResponse {
    eventId: number,
    events: [MarketEvent],
    timestamp: number,
    timestampms: number
}

export type MarketDataResponse = MarketHeartbeatResponse | MarketUpdateResponse