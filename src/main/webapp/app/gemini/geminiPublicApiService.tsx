import { Observable, Subject, BehaviorSubject } from 'rxjs'
import { takeUntil, scan, filter, map, flatMap } from 'rxjs/operators'
import { WebSocketSubject } from 'rxjs/webSocket'
import { Book } from '../book'
import { PublicApiService, Trade } from '../publicApiService'
import { MarketDataResponse, MarketUpdateResponse } from './marketDataResponse'
import { MarketEvent, MarketDataChangeEvent, MarketDataTradeEvent } from './marketEvent'

const GEMINI_URI = 'wss://api.gemini.com/v1/marketdata/btcusd?heartbeat=true'

export class GeminiPublicApiService implements PublicApiService {
    readonly book: Book
    
    private _isConnected: boolean
    private _connected: Subject<boolean>
    private _disconnect: Subject<boolean>
    private _websocket: Observable<MarketDataResponse>
    private _trades: Subject<Trade>
    private _updates: Subject<Date>
    
    constructor() {
        console.debug("GeminiPublicApiService::constructor()")
        
        this.book = new Book()
        this._updates = new Subject<Date>()
        this._trades = new Subject<Trade>()
        this._disconnect = new Subject<boolean>()
        this._connected = new BehaviorSubject(false)
        this._connected.subscribe((connectionEstablished: boolean) => {
            console.debug("PublicAPI Connection Established = %s", connectionEstablished)
            this._isConnected = connectionEstablished
        })
        
        this.connect()
    }
    
    isConnected(): boolean {
        return this._isConnected
    }
    
    connected(): Observable<boolean> {
        return this._connected
    }
    
    private reconnect(): void {
        console.debug("GeminiPublicApiService::reconnect()")
        
        this.disconnect()
        this.connect()
    }
    
    private connect(): void {
        console.debug("GeminiPublicApiService::connect()")
        
        let websocket = new WebSocketSubject<MarketDataResponse>({
            url: GEMINI_URI,
            openObserver: {
                next: (openEvent) => {
                    console.debug("Websocket opened. %o", openEvent)
                    this._connected.next(true)
                }
            },
            closeObserver: {
                next: (closeEvent) => {
                    console.debug("Websocket closed. %o", closeEvent)
                    this.reconnect()
                }
            }
        })
        
        this._websocket = websocket.pipe(takeUntil(this._disconnect))
        // websocket.subscribe(null, null, () => {
        //     console.log("websocket closed--unsubscribing")
        //     websocket.unsubscribe()
        // })

        // monitor for missed messages
        this._websocket.pipe(scan((a: MarketDataResponse[], b: MarketDataResponse) => {
            a.push(b)
            if (a.length > 2) {
                a.shift()
            }
            
            return a
        }, [{socket_sequence: -2}, {socket_sequence: -1}])).subscribe((value: MarketDataResponse[]) => {
            if (Math.abs(value[0].socket_sequence - value[1].socket_sequence) > 1) {
                console.warn("Sequence error. The following messages aren't sequential. %o %o", value[0], value[1])
                this.reconnect()
            }
        })
        
        // bookkeeping
        this.book.clear()
        let events = this._websocket.pipe(filter((value: MarketDataResponse) => {
            return value.type == "update"
        }), map((value: MarketUpdateResponse): MarketEvent[] => {
            return value.events
        }))
        
        // update entries
        events.pipe(flatMap((value) => {
            return value
        }), filter((value: MarketEvent) => {
            return value.type == "change"
        })).subscribe((value: MarketDataChangeEvent) => {
            this.book.addEntry(value)
        })
        
        // trigger updates
        events.subscribe((value: MarketEvent[]) => {
            this._updates.next(new Date())
        })
        
        // trade monitor
        this._websocket.pipe(filter((value: MarketDataResponse) => {
            return value.type == "update"
        }), map((value: MarketUpdateResponse): MarketEvent[] => {
            return value.events
        }), flatMap((value): MarketEvent[] => {
            return value
        }), filter((value: MarketEvent) => {
            return value.type == "trade"
        })).subscribe((value: MarketDataTradeEvent) => {
            this._trades.next(value)
        })
    }
    
    private disconnect(): void {
        console.debug("GeminiPublicApiService::disconnect()")
        
        this._disconnect.next(true)
        this._connected.next(false)
        
        console.log("Disconnected with %s entries", this.book.size())
    }
    
    trades(): Observable<Trade> {
        return this._trades
    }
    
    updates(): Observable<Date> {
        return this._updates
    }
}