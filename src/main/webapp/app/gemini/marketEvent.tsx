export interface AbstractMarketEvent {
    type: string
}

export interface MarketDataChangeEvent extends AbstractMarketEvent {
    price: number,
    side: string,
    reason: string,
    remaining: number,
    delta: number
}

export interface MarketDataTradeEvent extends AbstractMarketEvent {
    price: number,
    amount: number,
    markerSide: string
}

export interface MarketDataBlockTradeEvent extends AbstractMarketEvent {
    price: number,
    amount: number
}

export interface AbstractMarketDataAuctionEvent {
    type: string
}

export interface MarketDataAuctionOpenEvent extends AbstractMarketDataAuctionEvent {
    auction_open_ms: number,
    auction_time_ms: number,
    first_indicative_ms: number,
    last_cancel_time_ms: number
}

export interface MarketDataAuctionIndicativeEvent extends AbstractMarketDataAuctionEvent {
    eid: number,
    result: string,
    time_ms: number,
    highest_bid_price: number,
    lowest_ask_price: number,
    collar_price: number,
    indicative_price: number,
    indicative_quantity: number
}

export interface MarketDataAuctionOutcomeEvent extends AbstractMarketDataAuctionEvent {
    eid: number,
    result: string,
    time_ms: number,
    highest_bid_price: number,
    lowest_ask_price: number,
    collar_price: number,
    auction_price: number,
    auction_quantity: number
}

export type MarketDataAuctionEvent = MarketDataAuctionOpenEvent | MarketDataAuctionIndicativeEvent | MarketDataAuctionOutcomeEvent
export type MarketEvent = MarketDataChangeEvent | MarketDataTradeEvent | MarketDataBlockTradeEvent | MarketDataAuctionEvent