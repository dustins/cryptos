import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Cryptos } from './cryptos'

ReactDOM.render(
    <Cryptos />,
    document.getElementById("cryptos")
);