import { Observable } from 'rxjs'
import { Book } from './book'

export interface Trade {
    price: number,
    amount: number,
    markerSide: string
}

export interface PublicApiService {
    readonly book: Book
    
    isConnected(): boolean
    connected(): Observable<boolean>
    
    trades(): Observable<Trade>
    
    updates(): Observable<Date>
}