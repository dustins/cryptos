import * as React from 'react'
import { GeminiPublicApiService } from './gemini/geminiPublicApiService'
import { PublicApiService } from './publicApiService'

export interface CryptosContext {
    publicApi: PublicApiService
}

export function createCryptosContext(): CryptosContext {
    return {
        'publicApi': new GeminiPublicApiService()
    }
}

export const CryptosContext = React.createContext({});